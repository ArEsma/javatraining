package com.example.bootjpa;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableWebSecurity
public class AppSecurityController extends WebSecurityConfigurerAdapter {

    @Value("${my.admin.username}")
    private String username;
    @Value("${my.admin.password}")
    private String password;

    @Bean
    @Override
    protected UserDetailsService userDetailsService() {
        System.out.println(username);
        System.out.println(password);
        List<UserDetails> users = new ArrayList<>();
        users.add(User.withDefaultPasswordEncoder().username(username).password(password).roles("USER").build());
        return new InMemoryUserDetailsManager(users);
    }
}






