package com.example.rpctest.Rabbit;

public class SimpleRPCImpl implements SimpleRPC{

    @Override
    public Integer multByTwo(int n)
    {
        return 2 * n;
    }

}
