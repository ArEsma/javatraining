package com.example.bootjpa.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


//@Table(name = "schooltable")
@Entity
public class School {
    @Id
    private int sid;
    private String sname;
    private Coordinate sloc;

    public School() {
        this.sloc = new Coordinate(0, 0);
    }

    @OneToMany
    private List<Alien> students = new ArrayList<Alien>();

    @JsonManagedReference
    public List<Alien> getStudents() {
        return students;
    }

    public void setStudents(List<Alien> students) {
        this.students = students;
    }

    public void addToStudents(Alien student)
    {
        this.students.add(student);
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public Coordinate getSloc() {
        return sloc;
    }

    public void setSloc(Coordinate sloc) {
        this.sloc = sloc;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }
}
