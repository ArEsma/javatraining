package com.example.bootjpa.repos;

import com.example.bootjpa.model.Student;

import java.util.Map;

public interface StudentRepo {
    void save(Student student);
    Map<String, Student> findAll();

}
