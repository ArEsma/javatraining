package com.example.bootjpa.repos;

import com.example.bootjpa.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class StudentRepoImpl implements StudentRepo{

    private RedisTemplate<String, Student> redisTemplate;
    private HashOperations hashOperations;

    public StudentRepoImpl(RedisTemplate<String, Student> redisTemplate) {
        this.redisTemplate = redisTemplate;
        hashOperations = redisTemplate.opsForHash();
    }

    @Override
    public void save(Student student)
    {
        hashOperations.put("STUDENT", student.getName(), student);
    }
    @Override
    public Map<String, Student> findAll()
    {
        return hashOperations.entries("STUDENT");
    }
}
