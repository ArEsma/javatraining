package com.example.bootjpa.jts;


import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Polygon;

public class JtsExample {

    public  static void main(String[] argv) {

        Polygon polygon = new GeometryFactory().createPolygon(
                new Coordinate[]{new Coordinate(1,1),
                        new Coordinate(6,1), new Coordinate(6,6),
                        new Coordinate(1,6), new Coordinate(1,1)});
        LineString line = new GeometryFactory().createLineString(
                new Coordinate[]{new Coordinate(0, 0),
                        new Coordinate(5,5), new Coordinate(10,5)});

        if(line.intersects(polygon))
        {
            System.out.println("Line intersects polygon!");
        }
        else
        {
            System.out.println("Line doesn't intersect polygon!");
        }

    }

}
