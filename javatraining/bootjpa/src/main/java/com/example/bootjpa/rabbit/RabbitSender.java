package com.example.bootjpa.rabbit;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/*
@RestController
@RequestMapping("/send")
public class RabbitSender {

    private RabbitTemplate template;

    public RabbitSender(RabbitTemplate template) {
        this.template = template;
    }

    @RequestMapping("/{n}")
    public String order(@PathVariable Integer n) {
        Integer res = (Integer) template.convertSendAndReceive(RabbitConfig.EXCHANGE, RabbitConfig.ROUTING_KEY, n);
        System.out.println(res);
        return "Done!";
    }
}

 */
