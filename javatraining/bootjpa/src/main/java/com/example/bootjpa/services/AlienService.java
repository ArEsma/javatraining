package com.example.bootjpa.services;

import com.example.bootjpa.model.Alien;
import com.example.bootjpa.model.School;
import com.example.bootjpa.repos.AlienRepo;
import com.example.bootjpa.repos.SchoolRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.minio.MinioClient;
import io.minio.errors.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AlienService {

    AlienRepo repo;
    SchoolRepo srepo;
    MinioClient minioClient;

    public AlienService(AlienRepo repo, SchoolRepo srepo, MinioClient minioClient) {
        this.repo = repo;
        this.srepo = srepo;
        this.minioClient = minioClient;
    }

    @Value("${my.file.path}")
    String filePath;

    @Value("${minio.bucket.name}")
    String defaultBucketName;

    @Value("${minio.default.folder}")
    String defaultBaseFolder;

    public void getStudentsMap()
    {
        List<School> schools = srepo.findAll();
        Map<String, List<Alien>> schoolsMap = new HashMap<>(); //stream collect
        //schools.stream().forEach(school-> schoolsMap.put(school.getSname(), school.getStudents()));
        schoolsMap = schools.stream().collect(Collectors.toMap(School::getSname, School::getStudents));
        System.out.println("Hi");
        schoolsMap.entrySet().stream().forEach(e-> System.out.println(e));
    }

    @Async
    public void addJson(Alien student, int idx) throws IOException, InvalidArgumentException, InvalidBucketNameException, InsufficientDataException, XmlPullParserException, ErrorResponseException, NoSuchAlgorithmException, NoResponseException, InvalidKeyException, InternalException {
        ObjectMapper objectMapper = new ObjectMapper();
        System.out.println(idx);
        System.out.println(Thread.currentThread().getName());
        String name = String.format("/student%d.json", idx + 1);
        File file = new File(filePath + name);
        objectMapper.writeValue(file, student);
        minioClient.putObject(defaultBucketName, defaultBaseFolder + name, file.getAbsolutePath());

    }
}
