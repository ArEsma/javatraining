package com.example.bootjpa.controller;

import com.example.bootjpa.component.BeansComponent;
import com.example.bootjpa.configProps.SampleConfigProps;
import com.example.bootjpa.model.Alien;
import com.example.bootjpa.model.DogImage;
import com.example.bootjpa.model.Student;
import com.example.bootjpa.rabbit.SimpleRPC;
import com.example.bootjpa.repos.AlienRepo;
import com.example.bootjpa.repos.StudentRepo;
import com.example.bootjpa.services.AlienService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.minio.errors.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.xmlpull.v1.XmlPullParserException;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class AdminController {

    AlienRepo repo;
    AlienService service;
    private final RestTemplate restTemplate;
    SimpleRPC simpleRPC;
    StudentRepo studentRepo;
    SampleConfigProps smple;

    public AdminController(AlienRepo repo, AlienService service, RestTemplate restTemplate, StudentRepo studentRepo, SampleConfigProps smple, SimpleRPC simpleRPC) {
        this.repo = repo;
        this.service = service;
        this.restTemplate = restTemplate;
        this.studentRepo = studentRepo;
        this.smple = smple;
        this.simpleRPC = simpleRPC;
    }

    @Value("${my.api.url}")
    String apiUrl;
    @Value("${my.file.path}")
    String filePath;

    @RequestMapping("/admin")
    public ModelAndView home()
    {
        return new ModelAndView("admin.html");
    }

    @RequestMapping("/filterStudents")
    public List<String> filterStudents(@RequestParam int grade)
    {
        List <Alien> students = repo.findAll();
        Map<String, Integer> studentsMap = new HashMap<>();
        for(Alien student : students)
        {
            studentsMap.put(student.getAname(), student.getAgrade());
            Student tmp_student = new Student();
            tmp_student.setName(student.getAname());
            tmp_student.setGrade(student.getAgrade());
            studentRepo.save(tmp_student);
        }
        List<String> results = studentsMap.entrySet().stream()
                .filter(e -> e.getValue() > grade)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        return results;
    }

    @RequestMapping("/dog")
    public ModelAndView getDog(){
        ObjectMapper objectMapper = new ObjectMapper();
        Object dog = restTemplate.getForObject(apiUrl, Object.class);
        DogImage img = objectMapper.convertValue(dog, DogImage.class);
        String res = img.getMessage();
        return new ModelAndView("redirect:" + res);
    }

    @RequestMapping("/addJson")
    public String addJson() throws IOException, InterruptedException, InvalidArgumentException, InvalidBucketNameException, InsufficientDataException, XmlPullParserException, ErrorResponseException, NoSuchAlgorithmException, NoResponseException, InvalidKeyException, InternalException {
        List<Alien> students = repo.findAll();
        for(int i = 0; i < students.size(); i++)
        {
            Alien student = students.get(i);
            service.addJson(student, i);
        }
        return "Done";
    }

    @Scheduled(fixedRate = 30000L)
    void averageGrades()
    {
        double avg;
        List<Alien> students = repo.findAll();
        int sz = students.size();
        avg = students.stream().mapToDouble(Alien::getAgrade).sum();
        if(avg == 0){System.out.println(0);}
        else{avg = avg / (double) sz; System.out.println(avg);}
    }

    @PostConstruct
    void studentsMapPrint()
    {
        service.getStudentsMap();
        System.out.println(smple.getUsername());
        System.out.println(simpleRPC.multByTwo(2));
    }

}

@Configuration
@EnableScheduling
@ConditionalOnProperty(name = "scheduling.enabled", matchIfMissing = true)
class SchedulingConfiguration{

}
