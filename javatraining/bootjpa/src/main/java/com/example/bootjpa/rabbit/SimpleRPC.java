package com.example.bootjpa.rabbit;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public interface SimpleRPC {
    Integer multByTwo(int n);
}
